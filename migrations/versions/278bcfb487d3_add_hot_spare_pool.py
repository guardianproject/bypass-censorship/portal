"""Add hot spare pool

Revision ID: 278bcfb487d3
Revises: 2d747ffb9928
Create Date: 2023-05-30 16:08:37.770371

"""
from alembic import op
from sqlalchemy.sql import text

# revision identifiers, used by Alembic.
revision = '278bcfb487d3'
down_revision = '2d747ffb9928'
branch_labels = None
depends_on = None


def upgrade():
    # Add SQL here
    if op.get_context().dialect.name == 'postgresql':
        sql = text("""INSERT INTO pool VALUES (
                             -1, 'Hot spares (reserve)', NOW(), NOW(), NULL, 'hotspare', 
                             md5(to_char(NOW(), 'YYYY-MM-DD HH24:MI:SS.US')), NULL) ON CONFLICT (id) DO NOTHING;""")
    elif op.get_context().dialect.name == 'sqlite':
        sql = text(f"""INSERT OR IGNORE INTO pool VALUES (
                            -1, 'Hot spares (reserve)', datetime('now'), datetime('now'), NULL, 'hotspare',
                            CAST(RANDOM() AS TEXT), NULL);""")
    else:
        raise RuntimeError("Using an unsupported engine, only SQLite or PostgreSQL are supported. "
                           "You could do this migration manually.")
    op.execute(sql)


def downgrade():
    # SQL to undo the changes
    sql = text("""DELETE FROM pool WHERE id = -1;""")
    op.execute(sql)
