Introduction
============

Censorship is the suppression of speech, public communication, or other information.
This may be done on the basis that such material is considered objectionable, harmful, sensitive, or "inconvenient".
Regardless of the reasons for censorship, the technical measures taken to implement it often look the same.
The *Bypass Censorship* portal provides a toolkit for circumventing censorship of Internet resources.
The circumvention methods available will often exploit either collateral freedom, traffic obfuscation, or both in order
to counter the measures put in place by the censor.

Collateral Freedom
------------------

*Used by:* :doc:`Web Mirrors <mirrors>`, :doc:`Tor Bridges <bridges>`

"Collateral freedom" is an anti-censorship strategy that attempts to make it economically prohibitive for censors to
block an Internet resource.
The way in which a censor restricts access to resources will require knowing which content to block and which to allow.
It's incredibly difficult to achieve accuracy with filtering as the Internet is comprised of untagged free-form content
that must be categorised at speed.
This results in either over-blocking or under-blocking, and neither of these are desirable properties for the censor.

This can be exploited by circumvention systems by deploying solutions at places that are "too big to block", like cloud
providers.
Either encryption or constantly rotating identifiers are then used to prevent censors from identifying requests for
censored information that is hosted among other content.
This forces censors to either allow access to the censored information or take down entire services.

Traffic Obfuscation
-------------------

*Used by:* :doc:`Tor Bridges <bridges>`

"Traffic Obfuscation" is an anti-censorship strategy that attempts to make it difficult to fingerprint traffic.
This is more commonly used for general censorship circumvention solutions rather than means of accessing specific
resources.
There is a long tail of types of traffic on the Internet, including critical infrastructure communications like
industrial control systems, point-of-sale systems and security systems.
This can be exploited by circumvention systems by making their traffic look like one of these unclassified systems.
Not being able to accurately identify the traffic means that the cost of blocking access is unknown, and so it is more
difficult for a censor to justify the block.
