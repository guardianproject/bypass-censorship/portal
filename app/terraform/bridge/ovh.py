from app.models.cloud import CloudProvider
from app.terraform.bridge import BridgeAutomation


class BridgeOvhAutomation(BridgeAutomation):
    short_name = "bridge_ovh"
    description = "Deploy Tor bridges on OVH Public Cloud"
    provider = CloudProvider.OVH

    template_parameters = ["ssh_public_key_path", "ssh_private_key_path"]

    template = """
    terraform {
      {{ backend_config }}
      required_providers {
        random = {
          source = "hashicorp/random"
          version = "3.1.0"
        }
        openstack = {
          source  = "terraform-provider-openstack/openstack"
          version = "~> 1.42.0"
        }
        ovh = {
          source  = "ovh/ovh"
          version = ">= 0.13.0"
        }
      }
    }

    locals {
      public_ssh_key = "{{ ssh_public_key_path }}"
      private_ssh_key = "{{ ssh_private_key_path }}"
    }

    {% for resource in destroyed_resources %}
    {% set bridge, bridgeconf, account = resource %}
    provider "openstack" {
      auth_url    = "https://auth.cloud.ovh.net/v3/"
      domain_name = "Default" # Domain name - Always at 'default' for OVHcloud
      user_name   = "{{ account.credentials["ovh_openstack_user"] }}"
      password    = "{{ account.credentials["ovh_openstack_password"] }}"
      tenant_id   = "{{ account.credentials["ovh_openstack_tenant_id"] }}"
      alias       = "account_{{ bridge.id }}"
    }
    {% endfor %}

    {% for resource in active_resources %}
    {% set bridge, bridgeconf, account = resource %}
    provider "openstack" {
      auth_url    = "https://auth.cloud.ovh.net/v3/"
      domain_name = "Default" # Domain name - Always at 'default' for OVHcloud
      user_name   = "{{ account.credentials["ovh_openstack_user"] }}"
      password    = "{{ account.credentials["ovh_openstack_password"] }}"
      tenant_id   = "{{ account.credentials["ovh_openstack_tenant_id"] }}"
      alias       = "account_{{ bridge.id }}"
    }

    provider "ovh" {
      endpoint           = "ovh-eu"
      application_key    = "{{ account.credentials["ovh_cloud_application_key"] }}"
      application_secret = "{{ account.credentials["ovh_cloud_application_secret"] }}"
      consumer_key       = "{{ account.credentials["ovh_cloud_consumer_key"] }}"
      alias              = "account_{{ bridge.id }}"
    }

    data "ovh_cloud_project_regions" "regions_{{ bridge.id }}" {
      provider        = ovh.account_{{ bridge.id }}
      service_name    = "{{ account.credentials["ovh_openstack_tenant_id"] }}"
      has_services_up = ["instance"]
    }

    resource "random_shuffle" "region_{{ bridge.id }}" {
      input = data.ovh_cloud_project_regions.regions_{{ bridge.id }}.names
      result_count = 1

      lifecycle {
        ignore_changes = [input] # don't replace all the bridges if a new region appears
      }
    }

    module "bridge_{{ bridge.id }}" {
      providers = {
        openstack = openstack.account_{{ bridge.id }}
      }
      source = "{{ terraform_modules_path }}/terraform-openstack-tor-bridge"
      region = one(random_shuffle.region_{{ bridge.id }}.result)
      namespace = "{{ global_namespace }}"
      name = "bridge"
      attributes = ["{{ bridge.id }}"]
      ssh_public_key = local.public_ssh_key
      ssh_private_key = local.private_ssh_key
      contact_info = "hello from onionland"
      distribution_method = "{{ bridgeconf.method }}"
    }

    output "bridge_hashed_fingerprint_{{ bridge.id }}" {
      value = module.bridge_{{ bridge.id }}.hashed_fingerprint
    }

    output "bridge_bridgeline_{{ bridge.id }}" {
      value = module.bridge_{{ bridge.id }}.bridgeline
      sensitive = true
    }
    {% endfor %}
    """
