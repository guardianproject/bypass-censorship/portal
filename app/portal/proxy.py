from flask import Blueprint, Response, flash, redirect, render_template, url_for
from flask.typing import ResponseReturnValue
from sqlalchemy import desc

from app.extensions import db
from app.models.mirrors import Proxy
from app.portal.util import LifecycleForm

bp = Blueprint("proxy", __name__)


@bp.route("/list")
def proxy_list() -> ResponseReturnValue:
    proxies = (
        Proxy.query.filter(Proxy.destroyed.is_(None)).order_by(desc(Proxy.added)).all()
    )
    return render_template(
        "list.html.j2", section="proxy", title="Proxies", item="proxy", items=proxies
    )


@bp.route("/expire/<proxy_id>", methods=["GET", "POST"])
def proxy_expire(proxy_id: int) -> ResponseReturnValue:
    proxy = Proxy.query.filter(Proxy.id == proxy_id, Proxy.destroyed.is_(None)).first()
    if proxy is None:
        return Response(
            render_template(
                "error.html.j2",
                header="404 Proxy Not Found",
                message="The requested proxy could not be found. It may have already been "
                "destroyed.",
            )
        )
    form = LifecycleForm()
    if form.validate_on_submit():
        proxy.destroy()
        db.session.commit()
        flash("Proxy will be shortly retired.", "success")
        return redirect(url_for("portal.origin.origin_edit", origin_id=proxy.origin.id))
    return render_template(
        "lifecycle.html.j2",
        header=f"Expire proxy for {proxy.origin.domain_name} immediately?",
        message=proxy.url,
        section="proxy",
        form=form,
    )


@bp.route("/block/<proxy_id>", methods=["GET", "POST"])
def proxy_block(proxy_id: int) -> ResponseReturnValue:
    proxy = Proxy.query.filter(Proxy.id == proxy_id, Proxy.destroyed.is_(None)).first()
    if proxy is None:
        return Response(
            render_template(
                "error.html.j2",
                header="404 Proxy Not Found",
                message="The requested proxy could not be found. It may have already been "
                "destroyed.",
            )
        )
    form = LifecycleForm()
    if form.validate_on_submit():
        proxy.deprecate(reason="manual")
        db.session.commit()
        flash("Proxy will be shortly replaced.", "success")
        return redirect(url_for("portal.origin.origin_edit", origin_id=proxy.origin.id))
    return render_template(
        "lifecycle.html.j2",
        header=f"Mark proxy for {proxy.origin.domain_name} as blocked?",
        message=proxy.url,
        section="proxy",
        form=form,
    )
