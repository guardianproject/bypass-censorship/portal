from typing import Optional

from flask import Blueprint, redirect
from flask.typing import ResponseReturnValue

from app.models.onions import Onion
from app.portal.util import response_404, view_lifecycle

bp = Blueprint("onion", __name__)


@bp.route("/new", methods=["GET", "POST"])
@bp.route("/new/<group_id>", methods=["GET", "POST"])
def onion_new(group_id: Optional[int] = None) -> ResponseReturnValue:
    return redirect("/ui/web/onions/new")


@bp.route("/edit/<onion_id>", methods=["GET", "POST"])
def onion_edit(onion_id: int) -> ResponseReturnValue:
    return redirect("/ui/web/onions/edit/{}".format(onion_id))


@bp.route("/list")
def onion_list() -> ResponseReturnValue:
    return redirect("/ui/web/onions")


@bp.route("/destroy/<onion_id>", methods=["GET", "POST"])
def onion_destroy(onion_id: str) -> ResponseReturnValue:
    onion = Onion.query.filter(
        Onion.id == int(onion_id), Onion.destroyed.is_(None)
    ).first()
    if onion is None:
        return response_404("The requested onion service could not be found.")
    return view_lifecycle(
        header=f"Destroy onion service {onion.onion_name}",
        message=onion.description,
        success_message="Successfully removed onion service.",
        success_view="portal.onion.onion_list",
        section="onion",
        resource=onion,
        action="destroy",
    )
