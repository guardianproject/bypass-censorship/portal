from typing import Optional

from sqlalchemy.orm import Mapped, mapped_column

from app.extensions import db


class TerraformState(db.Model):  # type: ignore
    key: Mapped[str] = mapped_column(db.String, primary_key=True)
    state: Mapped[str]
    lock: Mapped[Optional[str]]
