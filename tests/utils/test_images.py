import pytest
from PIL import Image, ImageDraw
from app.brm.utils import thumbnail_uploaded_image, create_data_uri
from werkzeug.datastructures import FileStorage
from io import BytesIO
import base64


def create_test_image(image_format: str, extension: str):
    """
    Creates a test image with the data stored in a BytesIO buffer.

    :return: A test image.
    """
    img_byte = BytesIO()
    img = Image.new('RGB', (500, 500), color=(73, 109, 137))
    d = ImageDraw.Draw(img)
    d.text((10, 10), "Hello World", fill=(255, 255, 0))
    img.save(img_byte, format="jpeg")
    img_byte.seek(0)
    file = FileStorage(stream=img_byte, filename='test.jpg')
    return file


@pytest.mark.parametrize("image_format, extension", [
    ("jpeg", "jpg"),
    ("png", "png"),
    ("webp", "webp"),
])
def test_thumbnail_uploaded_image(image_format: str, extension: str):
    file = create_test_image(image_format, extension)
    output = thumbnail_uploaded_image(file)
    image = Image.open(BytesIO(output))

    # Check if the processed image size is as expected
    assert image.size[0] <= 256
    assert image.size[1] <= 256


@pytest.mark.parametrize("image_format, extension", [
    ("jpeg", "jpg"),
    ("png", "png"),
    ("webp", "webp"),
])
def test_create_data_uri(image_format: str, extension: str):
    file = create_test_image(image_format, extension)
    img_bytes = thumbnail_uploaded_image(file)
    file_extension = 'jpg'
    data_uri = create_data_uri(img_bytes, file_extension)

    # Check if the data URI starts as expected
    assert data_uri.startswith('data:image/jpeg;base64,')

    # Check if the base64 image can be decoded
    try:
        base64_data = data_uri.split(',')[1]
        base64.b64decode(base64_data)
    except Exception as e:
        pytest.fail(f"Decoding base64 data URI failed with error {e}")
