#!/bin/sh

set -e

nlines={1..$(flask db history | wc -l)}

# Upgrade to latest db schema
for _ in $nlines
do
  flask db upgrade
done

# Downgrade to the first schema
for _ in $nlines
do
  flask db downgrade
done

# Upgrade to the latest version again
for _ in $nlines
do
  flask db upgrade
done